import { Universe } from 'wasm-game-of-life';
import { create } from 'zustand';
import { memory } from '../../pkg/wasm_game_of_life_bg.wasm';

interface GameStore {
    cellEvolutionInterval: number; // unitless value
    isGameRunning: boolean;
    universe: Universe;
    toggleIsGameRunning: () => void;
}

const useGameStore = create<GameStore>((set) => ({
    cellEvolutionInterval: 50,
    isGameRunning: false,
    universe: Universe.new(),
    toggleIsGameRunning: () => set((state) => ({ isGameRunning: !state.isGameRunning }))
}));

export const useCells = (state: GameStore) =>
    new Uint8Array(
        memory.buffer,
        state.universe.cells(),
        state.universe.width() * state.universe.height()
    );

export default useGameStore;
