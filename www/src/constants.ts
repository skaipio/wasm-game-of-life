export const CELL_SIZE = 10; // px
export const DEAD_COLOR = '#FFFFFF';
export const ALIVE_COLOR = '#000000';
export const GRID_COLOR = '#CCCCCC';
