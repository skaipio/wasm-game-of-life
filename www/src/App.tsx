import { Stage } from '@pixi/react';
import * as PIXI from 'pixi.js';
import GameStage from './GameStage';
import { CELL_SIZE } from './constants';
import useGameStore from './store';

// Construct the universe, and get its width and height.

function App() {
    const { isGameRunning, universe, toggleIsGameRunning } = useGameStore();
    const width = (CELL_SIZE + 1) * universe.width() + 1;
    const height = (CELL_SIZE + 1) * universe.height() + 1;
    const pixiStageOptions: PIXI.IApplicationOptions = {
        backgroundAlpha: 1,
        backgroundColor: 0x1099bb,
        clearBeforeRender: false,
        context: null,
        antialias: false,
        powerPreference: 'default',
        premultipliedAlpha: false,
        preserveDrawingBuffer: false,
        hello: false
    };

    return (
        <div className="App">
            <Stage width={width} height={height} options={pixiStageOptions}>
                <GameStage />
            </Stage>
            <div>
                <button id="btn-toggle-game" onClick={toggleIsGameRunning}>
                    {isGameRunning ? 'Stop game' : 'Start game'}
                </button>
            </div>
        </div>
    );
}

export default App;
