import { Container, Graphics, useApp, useTick } from '@pixi/react';
import * as PIXI from 'pixi.js';
import React, { useCallback, useEffect, useRef, useState } from 'react';
import { Cell } from 'wasm-game-of-life';
import { ALIVE_COLOR, CELL_SIZE, DEAD_COLOR, GRID_COLOR } from './constants';
import useGameStore, { useCells } from './store';

//type GameStageProps = {};

const GameStage: React.FC = () => {
    const app = useApp();
    const store = useGameStore();
    const { cellEvolutionInterval, isGameRunning, universe } = store;
    const cells = useCells(store);
    const [tickCounter, setTickCounter] = useState(0);
    const cellEvolutionTimer = useRef(0);

    const drawGrid = (line: PIXI.Graphics) => {
        line.lineStyle({ width: 1, color: GRID_COLOR, alpha: 1 });

        // Vertical lines.
        for (let i = 0; i <= universe.width(); i++) {
            line.moveTo(i * (CELL_SIZE + 1), 0);
            line.lineTo(i * (CELL_SIZE + 1) + 1, (CELL_SIZE + 1) * universe.height() + 1);
        }

        // Horizontal lines.
        for (let j = 0; j <= universe.height(); j++) {
            line.moveTo(0, j * (CELL_SIZE + 1));
            line.lineTo((CELL_SIZE + 1) * universe.width() + 1, j * (CELL_SIZE + 1) + 1);
        }
    };

    const drawCells = useCallback(
        (rect: PIXI.Graphics) => {
            const getIndex = (row: number, column: number) => {
                return row * universe.width() + column;
            };

            for (let row = 0; row < universe.height(); row++) {
                for (let col = 0; col < universe.width(); col++) {
                    const idx = getIndex(row, col);

                    rect.beginFill(cells[idx] === Cell.Dead ? DEAD_COLOR : ALIVE_COLOR);

                    rect.drawRect(
                        col * (CELL_SIZE + 1) + 1,
                        row * (CELL_SIZE + 1) + 1,
                        CELL_SIZE,
                        CELL_SIZE
                    );

                    rect.endFill();
                }
            }
        },
        [tickCounter]
    );

    useEffect(() => {
        if (isGameRunning) {
            app.start();
        } else {
            app.stop();
        }
    }, [isGameRunning]);

    useTick((delta: number) => {
        cellEvolutionTimer.current += delta;
        if (cellEvolutionTimer.current / cellEvolutionInterval >= 1) {
            universe.tick();
            setTickCounter(tickCounter + 1);
            cellEvolutionTimer.current -= cellEvolutionInterval;
        }
    });

    return (
        <Container>
            <Graphics draw={drawGrid} />
            <Graphics draw={drawCells} />
        </Container>
    );
};

export default GameStage;
