//const CopyWebpackPlugin = require("copy-webpack-plugin");
const HtmlWebpackPlugin = require('html-webpack-plugin');
const paths = require('./paths');

module.exports = {
    entry: './bootstrap.ts',
    output: {
        path: paths.appBuild,
        filename: 'bootstrap.js'
    },
    mode: 'development',
    resolve: {
        extensions: ['.ts', '.tsx', '.js']
    },
    module: {
        rules: [
            {
                test: /\.(ts|tsx)$/,
                use: 'ts-loader',
                include: paths.appSrc
            }
        ]
    },
    plugins: [
        // new CopyWebpackPlugin(["index.html"]),
        // Generates an `index.html` file with the <script> injected.
        new HtmlWebpackPlugin(
            Object.assign(
                {},
                {
                    inject: true,
                    template: paths.appHtml
                }
            )
        )
    ],
    experiments: {
        syncWebAssembly: true
    }
};
