# Game of Life with Rust and WebAssembly

## About

Built with 🦀🕸

[**📚 Read this template tutorial! 📚**][template-docs]

This template is designed for compiling Rust libraries into WebAssembly and
publishing the resulting package to NPM.

## 🚴 Usage

### 🛠️ Build with `wasm-pack build`

```
wasm-pack build
```

### 🔬 Test in Headless Browsers with `wasm-pack test`

```
wasm-pack test --headless --firefox
```

## 🔋 Batteries Included

* [`wasm-bindgen`](https://github.com/rustwasm/wasm-bindgen) for communicating
  between WebAssembly and JavaScript.
* [`console_error_panic_hook`](https://github.com/rustwasm/console_error_panic_hook)
  for logging panic messages to the developer console.
* `LICENSE-APACHE` and `LICENSE-MIT`: most Rust projects are licensed this way, so these are included for you

## Setup

Install cargo-binstall (Linux).

```shell
curl -L --proto '=https' --tlsv1.2 -sSf https://raw.githubusercontent.com/cargo-bins/cargo-binstall/main/install-from-binstall-release.sh | bash
```

Install cargo-watch.

```shell
cargo binstall cargo-watch
```

## Running

Do a cargo build every time there are changes in `./src`:
```shell
cargo watch -w ./src -s 'wasm-pack build'
```

Run in a Docker container
```shell
docker run --rm -t -p 8080:8080 wasm-game-of-life
```