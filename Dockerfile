FROM rust:slim-buster
ENV NODE_MAJOR=20

RUN cargo install wasm-pack
# Install node
RUN apt-get update
RUN apt-get install -y ca-certificates curl gnupg
RUN mkdir -p /etc/apt/keyrings
RUN curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
RUN echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | tee /etc/apt/sources.list.d/nodesource.list
RUN apt-get update
RUN apt-get install nodejs -y

WORKDIR /app
COPY . /app/

RUN wasm-pack build

WORKDIR /app/www
RUN npm install

CMD NODE_OPTIONS=--openssl-legacy-provider npm run start -- --host 0.0.0.0 --port 8080